<?php
/**
 * Created by Roman Myhailovich
 * Email: rmyhailovich@gmail.com
 * Date: 11/04/16
 * Time: 13:21 PM
 */


$installer = $this;

try{
    
// НАЧАЛО установки
    $installer->startSetup();

    $installer->run("ALTER TABLE
                       newsletter_subscriber
                       ADD COLUMN `subscriber_name`
                       VARCHAR(60) NULL DEFAULT NULL AFTER `subscriber_confirm_code`;");

    $installer->run("ALTER TABLE ".$subscribe."
                  ADD INDEX idx_subscriber_name
                  USING BTREE (subscriber_name ASC) ");

// КОНЕЦ установки
    $installer->endSetup();

}catch (Exception $e){
    Mage::log($e->getMessage(), Zend_Log::ERR, 'rm_subscribe', true);
}




